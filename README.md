# SpringBootDemo

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是开源中国推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
这是一个个人学习SpringBoot整合的记录。如果有不对的地方,希望各位能够指导出来。
会持续学习


#### 安装教程

#### 使用说明

1.boot-redis:是一个与MyBatis和Redis整合的Demo案例,其中还有使用MyBatis的自动生成配置。<br/>
2.boot-mongo:是一个与Mongo进行整合的Demo案列。具体的CRUD写在Test下面,没有使用接口的方式去写。<br/>
3.boot-swagger:是一个SpringBoot与Swagger整合的小案例。当启动项目的时候,访问比如:localhost:8989/swagger-ui.html;既可以可看到你swagger的页面。<br/>
4.boot-druid:是一个SpringBoot与druid整合的方案。@ConfigurationProperties(prefix = “spring.datasource”)
这个注解可以直接注入下面的,就不需要一个一个的@Value那样的操作,减轻了代码量。启动项目:访问http://localhost:8989/druid/login.html  输入你再DruidStatViewServlet中配置的账号和密码即可,然后你就可以看到管理的界面。<br/>
5:boot-fastdfs是一个整合fastdfs与SpringBoot进行整合的操作。具体的操作我放在测试类山,通过Controller那种上传写完了,但是还是有待测试(此时还未测试)<br/>
6:boot-redis-lock是一个基于Redis实现的简单的分布式操作。使用jmeter测压工具来同时测二个接口，会出现不同的结果现象。<br/>
#### 参与贡献
