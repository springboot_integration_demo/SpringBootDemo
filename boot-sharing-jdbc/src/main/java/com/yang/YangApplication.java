package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/17 11:30
 * @Version 1.0
 */

@SpringBootApplication
public class YangApplication {

    public static void main(String[] args) {
        SpringApplication.run(YangApplication.class,args);
    }

}
