package com.iyang.bootdubboserver.model;

import java.io.Serializable;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 0:17
 * @Version 1.0
 */
public class User implements Serializable {

    private static final long serialVersionUID = -6672313663778960926L;
    private Long id;
    private String userName;
    private String phone;

    public User() {
    }

    public User(Long id, String userName, String phone) {
        this.id = id;
        this.userName = userName;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
