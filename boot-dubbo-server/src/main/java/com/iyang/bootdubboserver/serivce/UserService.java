package com.iyang.bootdubboserver.serivce;

import com.iyang.bootdubboserver.model.User;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 0:19
 * @Version 1.0
 */
public interface UserService {
    public User getUserAll();
}
