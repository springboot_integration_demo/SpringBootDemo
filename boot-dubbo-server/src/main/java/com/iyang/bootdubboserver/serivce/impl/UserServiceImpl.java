package com.iyang.bootdubboserver.serivce.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.iyang.bootdubboserver.model.User;
import com.iyang.bootdubboserver.serivce.UserService;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 0:20
 * @Version 1.0
 */

@Service(version = "1.0.0")
public class UserServiceImpl implements UserService {
    @Override
    public User getUserAll() {
        return new User(1L,"MuYi","哈哈哈");
    }
}
