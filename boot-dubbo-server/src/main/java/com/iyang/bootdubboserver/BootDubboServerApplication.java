package com.iyang.bootdubboserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootDubboServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootDubboServerApplication.class, args);
    }

}
