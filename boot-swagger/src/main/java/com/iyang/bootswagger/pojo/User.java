package com.iyang.bootswagger.pojo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/9 17:29
 * @Version 1.0
 */

@ApiModel(value = "用户是实体类")
public class User implements Serializable {

    @ApiModelProperty(value = "用户Id")
    private Long id;
    @ApiModelProperty(value = "用户名字")
    private String name;
    @ApiModelProperty(value = "用户密码")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
