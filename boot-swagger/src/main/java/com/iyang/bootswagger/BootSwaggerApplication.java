package com.iyang.bootswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.iyang"})
public class BootSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootSwaggerApplication.class, args);
    }

}
