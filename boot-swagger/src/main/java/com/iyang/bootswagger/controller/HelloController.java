package com.iyang.bootswagger.controller;

import com.iyang.bootswagger.pojo.User;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/9 17:27
 * @Version 1.0
 */

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {

    @ApiOperation(value = "接受用户",notes = "保存用户")
    @ApiImplicitParam(name = "user",value = "用户信息",required = true,dataType = "User")
    @GetMapping()
    public String hello(@RequestBody User user){
        return "success";
    }

}
