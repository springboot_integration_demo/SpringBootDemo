package com.iyang.bootfastdfs;


import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootFastdfsApplicationTests {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Test
    public void testUpload() throws  Exception{
        File file = new File("E:\\图片\\1501923838534.jpeg");
        StorePath storePath = this.fastFileStorageClient.uploadFile(new FileInputStream(file), file.length(), "jpeg", null);
        System.out.println(storePath.getFullPath());
        System.out.println(storePath.getPath());
    }




}
