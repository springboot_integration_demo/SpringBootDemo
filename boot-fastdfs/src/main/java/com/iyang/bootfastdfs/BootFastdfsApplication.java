package com.iyang.bootfastdfs;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class BootFastdfsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootFastdfsApplication.class, args);
    }

}
