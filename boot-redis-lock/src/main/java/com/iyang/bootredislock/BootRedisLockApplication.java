package com.iyang.bootredislock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Mu_Yi
 * @Date 2019-04-07
 */
@SpringBootApplication
public class BootRedisLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootRedisLockApplication.class, args);
    }

}
