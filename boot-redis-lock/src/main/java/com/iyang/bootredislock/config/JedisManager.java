package com.iyang.bootredislock.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 自定义JedisManager类，用于管理jedisPool
 * @Author: Mu_YI
 * @Date: 2019/4/7 18:51
 * @Version 1.0
 */
public class JedisManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(JedisManager.class);
    private static final String REDIS_IP = "193.112.166.128";
    private static final Integer REDIS_PORT = 6379;
    private static JedisPool jedisPool;
    private static JedisPoolConfig config = new JedisPoolConfig();

    public static void init(){
        config.setMaxTotal(1000);
        config.setMaxIdle(10);
        config.setMaxWaitMillis(10*1000);
        //borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
        config.setTestOnBorrow(true);
        //return一个jedis实例给pool时，是否检查连接可用,设置为true时，返回的对象如果验证失败，将会被销毁，否则返回
        config.setTestOnReturn(true);
    }

    public static JedisPool getJedisPool(){
        if(jedisPool == null){
            synchronized (JedisManager.config){
                if(jedisPool == null){
                    init();
                    jedisPool = new JedisPool(config,REDIS_IP,REDIS_PORT,3000,"meng");
                    LOGGER.info("【Redis Lock】jedisPool初始化成功");
                    return jedisPool;
                }
            }
        }
        return jedisPool;
    }

}
