package com.iyang.bootredislock.pojo;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/7 19:16
 * @Version 1.0
 */
public class User {

    private Long id;
    private String name;
    private String password;
    private String phone;
    private Long age;

    public User(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }
}
