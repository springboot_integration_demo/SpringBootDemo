package com.iyang.bootredislock.mapper;

import com.iyang.bootredislock.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/4/7 19:17
 * @Version 1.0
 */

@Mapper
public interface UserMapper {

    @Select("select * from tb_user where id = #{id}")
    public User getById(Long id);

    @Update("update tb_user set age = #{age} where id = #{id}")
    public void updateAgeById(@Param("id") Long id,@Param("age") Long age);

}
