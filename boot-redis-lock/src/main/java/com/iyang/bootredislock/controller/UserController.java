package com.iyang.bootredislock.controller;

import com.iyang.bootredislock.service.RedisLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/4/7 19:24
 * @Version 1.0
 */

@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    private RedisLockService redisLockService;

    @GetMapping
    public void testRedisLockService(Long id, Long stock){
        redisLockService.updateStockById1(id,stock);
    }

    @GetMapping("no-lock")
    public void testNoLock(Long id,Long stock){
        redisLockService.updateNoLock(id,stock);
    }

}
