package com.iyang.bootredislock.service;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/4/7 19:10
 * @Version 1.0
 */

public interface RedisLockService {

    /**
     * 基于Redis锁的
     * @param id
     * @param stock
     */
    public void updateStockById1(Long id, Long stock);

    /**
     * 不是基于Redis锁的
     * @param id
     * @param stock
     */
    public void updateNoLock(Long id,Long stock);

}
