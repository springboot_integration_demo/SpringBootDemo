package com.iyang.bootredis.mapper;

import com.iyang.bootredis.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/6 22:59
 * @Version 1.0
 */

public interface UserMapper {

    /**
     * 根据id来删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 保存数据
     * @param record
     * @return
     */

    int insert(User record);

    /**
     * 选择插入
     * @param record
     * @return
     */
    int insertSelective(User record);

    /**
     * 根据id来查询
     * @param id
     * @return
     */
    User selectByPrimaryKey(Long id);

    /**
     * 根据id来跟新
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * 根据id来全部更新
     * @param record
     * @return
     */
    int updateByPrimaryKey(User record);

    /**
     * 自定义查询
     * @param name
     * @param pageNumber
     * @param pageSize
     * @param paging
     * @return
     */
    List<User> selectUser(@Param("name") String name, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize,@Param("paging") boolean paging);
}