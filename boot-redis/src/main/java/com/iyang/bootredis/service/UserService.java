package com.iyang.bootredis.service;

import com.iyang.bootredis.model.User;

import java.util.Map;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/6 22:59
 * @Version 1.0
 */
public interface UserService {

    boolean saveUser(User user);

    boolean updateUserById(User user);

    boolean deleteUserById(Long id);

    User selectUserById(Long id);

    Map<String, Object> selectUser(String name, int pageNumber, int pageSize, boolean paging);
}
