package com.iyang.bootredis.controller;

import com.iyang.bootredis.mapper.UserMapper;
import com.iyang.bootredis.model.User;
import com.iyang.bootredis.service.UserService;
import com.iyang.bootredis.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/6 22:59
 * @Version 1.0
 */

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Map<String,Object> selectUser(String name, @RequestParam(defaultValue = "0") int pageNumber, @RequestParam(defaultValue = "10")int pageSize, @RequestParam("true") boolean paging){
        return userService.selectUser(name,pageNumber,pageSize,paging);
    }

    @PostMapping
    public boolean saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }

    @PutMapping
    public boolean updateUserById(@RequestBody User user){
        return userService.updateUserById(user);
    }

    @DeleteMapping("{id}")
    public boolean deleteUserById(@PathVariable("id") Long id){
        return userService.deleteUserById(id);
    }

    @GetMapping("{id}")
    public User selectUserById(@PathVariable("id")Long id){
        return userService.selectUserById(id);
    }
}
