package com.iyang.bootredis.cache;

import com.iyang.bootredis.model.User;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/7 21:57
 * @Version 1.0
 */
public interface UserCache {

    void saveUser(User user);

    User selectUserById(Long id);
}
