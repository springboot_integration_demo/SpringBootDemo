package com.iyang.bootredis.cache.impl;

import com.iyang.bootredis.cache.UserCache;
import com.iyang.bootredis.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/7 21:58
 * @Version 1.0
 */

@Service
public class UserCacheImpl implements UserCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCacheImpl.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void saveUser(User user) {
        redisTemplate.opsForHash().put("user",String.valueOf(user.getId()),user);
    }

    @Override
    public User selectUserById(Long id) {
        if(redisTemplate.opsForHash().hasKey("user",String.valueOf(id))){
            LOGGER.info("走的缓存Redis拿的数据.");
            return (User) redisTemplate.opsForHash().get("user",String.valueOf(id));
        }
        return null;
    }
}
