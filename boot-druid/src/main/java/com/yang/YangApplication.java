package com.yang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/17 12:12
 * @Version 1.0
 */

@SpringBootApplication
@ServletComponentScan
public class YangApplication {

    public static void main(String[] args) {
        SpringApplication.run(YangApplication.class,args);
    }

}
