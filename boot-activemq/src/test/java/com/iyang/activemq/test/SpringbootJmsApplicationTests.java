package com.iyang.activemq.test;

import com.iyang.activemq.ActiveMqApplication;
import com.iyang.activemq.service.Porduct;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/5 23:26
 * @Version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ActiveMqApplication.class)
public class SpringbootJmsApplicationTests {

    @Autowired
    private Porduct porduct;

    @Test
    public void contextLoads() throws InterruptedException {
        Destination destination = new ActiveMQQueue("mytest.queue");
        for (int i = 0; i < 100; i++) {
            porduct.sendMessage(destination, "myname is Mu_Yi!!!" + i);
        }
        }
        }