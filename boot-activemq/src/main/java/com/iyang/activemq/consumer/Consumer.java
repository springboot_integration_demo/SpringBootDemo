package com.iyang.activemq.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 消费者
 * @Author: Mu_YI
 * @Date: 2019/4/5 23:23
 * @Version 1.0
 */

@Component
public class Consumer {

    @JmsListener(destination = "mytest.queue")
    public void receiveQueue(String text){
        System.out.println("Consumer:mytest.queue收到的报文为:" + text);
    }

}
