package com.iyang.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * @Author: Mu_YI
 * @Date: 2019/4/5 23:29
 * @Version 1.0
 */

@SpringBootApplication
public class ActiveMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActiveMqApplication.class,args);
    }

}
