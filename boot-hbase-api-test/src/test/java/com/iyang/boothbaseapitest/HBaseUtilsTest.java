package com.iyang.boothbaseapitest;

import com.iyang.boothbaseapitest.utils.HBaseUtils;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/13 22:54
 * @Version 1.0
 */
public class HBaseUtilsTest {

    @Test
    public void createTable(){
        HBaseUtils.createTable("FileTable",new String[]{"fileInfo","saveInfo"});
    }

    @Test
    public void addFileDetails(){
        HBaseUtils.putRow("FileTable","rowkey1","fileInfo","name","file1.txt");
        HBaseUtils.putRow("FileTable","rowkey1","fileInfo","type","txt");
        HBaseUtils.putRow("FileTable","rowkey1","fileInfo","size","1024");
        HBaseUtils.putRow("FileTable","rowkey1","saveInfo","creator","yang");
        HBaseUtils.putRow("FileTable","rowkey2","fileInfo","name","file1.txt");
        HBaseUtils.putRow("FileTable","rowkey2","fileInfo","type","txt");
        HBaseUtils.putRow("FileTable","rowkey2","fileInfo","size","1024");
        HBaseUtils.putRow("FileTable","rowkey2","saveInfo","creator","yang");
    }

    @Test
    public void getFileDetails(){
        Result result = HBaseUtils.getRow("FileTable", "rowkey1");
        if(result != null){
            System.out.println("rowkey=" + Bytes.toString(result.getRow()));
        }
    }


}
