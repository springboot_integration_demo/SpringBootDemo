package com.iyang.bootmongo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.iyang.bootmongo.model.User;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;
import org.bson.BSON;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootMongoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 添加
     */
    @Test
    public void addTest(){
        User user = new User(1L,"羊羊羊","12345567");
        Document document = new Document();
        mongoTemplate.save(user);

    }

    @Test
    public void select(){
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.append("_id",1L);
        MongoCursor<Document> iterator = mongoTemplate.getCollection("user").find(basicDBObject).iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next().toString());
        }
    }

    @Test
    public void update(){
        User user = new User(1L,"哈哈哈","12345567");
        mongoTemplate.update(user.getClass());
    }

    @Test
    public void delete(){
        BasicDBObject basicDBObject =  new BasicDBObject("_id",1L);
        long count = mongoTemplate.getCollection("user").deleteOne(basicDBObject).getDeletedCount();
        System.out.println(count);
    }

    @Test
    public void addInfoByBson(){
        Document document = new Document();
        document.put("id",1L);
        document.put("name","哈哈哈哈");
        document.put("phone","89899");
        mongoTemplate.getCollection("user").insertOne(document);
    }
}
