package com.iyang.bootmongo.model;

import java.io.Serializable;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/7 22:52
 * @Version 1.0
 */
public class User implements Serializable {
    private static final long serialVersionUID = 7035424790589970829L;

    private Long id;
    private String name;
    private String phone;

    public User() {
    }

    public User(Long id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
