package com.iyang.bootstreamrabbitmq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 23:18
 * @Version 1.0
 */

@Component
public class HelloListener {

    @RabbitListener(queues = "queue")
    public void proccessQueue(String str){
        System.out.println("监听收到的消息:" + str);
    }

}
