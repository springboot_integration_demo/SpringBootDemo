package com.iyang.bootstreamrabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 23:20
 * @Version 1.0
 */

@Configuration
public class SenderConf {

    @Bean
    public Queue queue(){
        return new Queue("queue");
    }

}
