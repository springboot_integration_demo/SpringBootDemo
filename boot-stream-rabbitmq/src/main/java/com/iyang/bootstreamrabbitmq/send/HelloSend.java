package com.iyang.bootstreamrabbitmq.send;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 23:16
 * @Version 1.0
 */

@Component
public class HelloSend {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(){
        amqpTemplate.convertAndSend("queue","Hello Rabit");
    }

}
