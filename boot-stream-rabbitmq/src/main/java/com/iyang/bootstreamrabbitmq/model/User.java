package com.iyang.bootstreamrabbitmq.model;

import java.io.Serializable;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/10 23:11
 * @Version 1.0
 */
public class User implements Serializable {
    private static final long serialVersionUID = -1809024934888273483L;

    private Long id;
    private String userName;
    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
