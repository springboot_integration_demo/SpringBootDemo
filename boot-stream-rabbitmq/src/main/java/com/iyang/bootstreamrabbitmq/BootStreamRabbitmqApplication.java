package com.iyang.bootstreamrabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootStreamRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootStreamRabbitmqApplication.class, args);
    }

}
